import pytest


def pytest_addoption(parser):
    parser.addoption("--test-file", action="store", default="pipes.stag.json")
    return parser


@pytest.fixture(scope='class')
def filename(request):
    setattr(request.cls, 'test_file', request.config.getoption('test_file'))
