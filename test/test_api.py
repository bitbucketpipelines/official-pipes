import unittest
from unittest.mock import patch, MagicMock

from official_build.api.bitbucket.base import BitbucketApiService
from test.fixtures import README, YAML_FRAGMENT


def mock_successful_response(json=None, text=None):
    response = MagicMock()
    response.status_code = 200
    response.text = text
    if json:
        response.json = MagicMock(return_value=json)
    return response


class BitbucketApiServiceTest(unittest.TestCase):

    @patch('official_build.api.bitbucket.base.requests.get')
    def test_extract_yml_definition_is_correct(self, mock_get):
        mock_get.return_value = mock_successful_response(text=README)

        service = BitbucketApiService()
        self.assertEqual(YAML_FRAGMENT, service.get_yml_definition(YAML_FRAGMENT, '1.0.0'))

    @patch('official_build.api.bitbucket.base.requests.get')
    def test_extract_yml_definition_fails(self, mock_get):
        mock_get.return_value = mock_successful_response(text="""### YAML Definition""")
        service = BitbucketApiService()
        self.assertEqual(None, service.get_yml_definition("""### YAML Definition""", '1.0.0'))

    @patch('official_build.api.bitbucket.base.requests.get')
    def test_latest_tag(self, mock_get):
        service = BitbucketApiService()
        mock_get.return_value = mock_successful_response(json={'pagelen': 100, 'values': [{'name': '0.3.1'}, {'name': '0.3.0'}]})

        tag = service.get_latest_tag('atlassian/aws-cloudformation-deploy')
        self.assertEqual('0.3.1', tag)

    @patch('official_build.api.bitbucket.base.requests.get')
    def test_latest_tag_order(self, mock_get):
        service = BitbucketApiService()
        mock_get.return_value = mock_successful_response(json={'pagelen': 100, 'values': [{'name': '0.10.1'}, {'name': '0.1.0'}]})

        tag = service.get_latest_tag('atlassian/aws-cloudformation-deploy')
        self.assertEqual('0.10.1', tag)
